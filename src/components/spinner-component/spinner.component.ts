import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'spinner-component',
  templateUrl: 'spinner.component.html'
})
export class SpinnerComponent implements OnInit {
  
  @Input() status: any;
  
  constructor() {
  }
  
  ngOnInit() {
  }
  
}
