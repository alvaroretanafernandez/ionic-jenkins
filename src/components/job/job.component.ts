import { Component, Input, OnInit } from '@angular/core';
import { NavController } from 'ionic-angular';
import { JobPage } from '../../pages/job/job';

@Component({
  selector: 'app-job',
  templateUrl: 'job.component.html'
})
export class JobComponent implements OnInit {
  
  @Input() job: any;
  
  constructor(public navCtrl: NavController) {
  }
  
  ngOnInit() {
  }
  
  goToJobDetail() {
    this.navCtrl.push(JobPage, { jobName: this.job.name });
  }
}
