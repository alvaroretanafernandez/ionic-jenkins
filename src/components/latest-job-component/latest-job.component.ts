import { Component, Input, OnInit } from '@angular/core';
import { JenkinsService } from '../../providers/jenkins-service/jenkins.service';
import { Observable } from 'rxjs';
import { BuildPage } from '../../pages/build/build';
import { NavController } from 'ionic-angular';

@Component({
    selector: 'latest-job-component',
    templateUrl: 'latest-job.component.html'
})
export class LatestJobsComponent implements OnInit {
  
  @Input() job: any;
  
  fullBuilds: any;
  
  constructor(public jenkinsService: JenkinsService,public navCtrl: NavController) { }

  ngOnInit() {
    console.log(this.job.builds);
    this.getBuildData()
      .subscribe((data) => {
        this.fullBuilds = data;
      })
  }
  
  getBuildData() {
    let callList = [];
    for (let build of this.job.builds){
      callList.push(this.jenkinsService.getBuild(this.job.name,build.number));
    }
    return Observable.forkJoin(callList);
  }
  goToBuild(build:any){
    this.navCtrl.push(BuildPage,{jobName:this.job.name,buildId:build});
  }
  
}
