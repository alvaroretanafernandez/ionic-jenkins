import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'job-subheader-component',
  templateUrl: 'job-subheader.component.html'
})
export class JobSubHeaderComponent implements OnInit {
  
  @Input() subheaderColor: any;
  @Input() jobName: any;
  
  constructor() {
  }
  
  ngOnInit() {
  }
  
}
