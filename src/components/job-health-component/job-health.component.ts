import { Component, Input, OnInit } from '@angular/core';

@Component({
    selector: 'job-health-component',
    templateUrl: 'job-health.component.html'
})
export class JobHealthComponent implements OnInit {
  
  @Input() description: any;
  @Input() lastUpdate: any;
  @Input() icon: any;
  
    constructor() { }

    ngOnInit() { }
    
}
