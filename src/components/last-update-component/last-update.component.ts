import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'last-update',
  templateUrl: 'last-update.component.html'
})
export class LastUpdateComponent implements OnInit {
  @Input() lastUpdate: any;
  
  constructor() {
  }
  
  ngOnInit() {
  }
  
}
