import { Component, Input, OnInit } from '@angular/core';

@Component({
    selector: 'job-build-report-component',
    templateUrl: 'job-build-report.component.html'
})
export class JobBuildReportComponent implements OnInit {
  
  @Input() title: any;
  @Input() jobNumber: any;
  @Input() color: any;
  
    constructor() { }

    ngOnInit() { }
    
}
