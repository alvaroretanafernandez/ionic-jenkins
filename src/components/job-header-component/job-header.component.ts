import { Component, Input, OnInit } from '@angular/core';

@Component({
    selector: 'job-header-component',
    templateUrl: 'job-header.component.html'
})
export class JobHeaderComponent implements OnInit {
  
  @Input() jobDisplayName: any;
  @Input() title: any;
  @Input() icon: any;
  
    constructor() { }

    ngOnInit() { }
    
}
