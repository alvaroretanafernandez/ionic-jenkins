import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'build-changes-component',
  templateUrl: 'build-changes.component.html'
})
export class BuildChangesComponent implements OnInit {
  
  @Input() build: any;
  
  constructor() {
  }
  
  ngOnInit() {
  }
  
}
