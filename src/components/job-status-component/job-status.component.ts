import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'job-status-component',
  templateUrl: 'job-status.component.html'
})
export class JobStatusComponent implements OnInit {
  
  @Input() status: any;
  
  constructor() {
  }
  
  ngOnInit() {
  }
  
}
