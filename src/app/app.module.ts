import { BrowserModule } from '@angular/platform-browser';
import { NgModule, ErrorHandler } from '@angular/core';

import { IonicApp, IonicModule, IonicErrorHandler } from 'ionic-angular';

import { InAppBrowser } from '@ionic-native/in-app-browser';
import { SplashScreen } from '@ionic-native/splash-screen';

import { IonicStorageModule } from '@ionic/storage';

import { ConferenceApp } from './app.component';

import { AboutPage } from '../pages/about/about';
import { AccountPage } from '../pages/account/account';
import { LoginPage } from '../pages/login/login';
import { TabsPage } from '../pages/tabs-page/tabs-page';
import { TutorialPage } from '../pages/tutorial/tutorial';

import { UserData } from '../providers/user-data';
import { UserService } from '../providers/user/user.service';
import { Auth } from '../providers/auth/auth.service';
import { JenkinsService } from '../providers/jenkins-service/jenkins.service';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { DashboardPage } from '../pages/dashboard/dashboard';
import { JobPage } from '../pages/job/job';
import { BuildPage } from '../pages/build/build';
import { DisclaimerComponent } from '../components/disclaimer/disclaimer.component';
import { JobComponent } from '../components/job/job.component';
import { JobHeaderComponent } from '../components/job-header-component/job-header.component';
import { JobHealthComponent } from '../components/job-health-component/job-health.component';
import { JobBuildReportComponent } from '../components/job-build-report-component/job-build-report.component';
import { LatestJobsComponent } from '../components/latest-job-component/latest-job.component';
import { LastUpdateComponent } from '../components/last-update-component/last-update.component';
import { JobStatusComponent } from '../components/job-status-component/job-status.component';
import { JobSubHeaderComponent } from '../components/job-subheader-component/job-subheader.component';
import { BuildChangesComponent } from '../components/build-changes-component/build-changes.component';
import { HttpClientModule } from '@angular/common/http';
import { InterceptorModule } from '../modules/interceptor.module';
import { SpinnerComponent } from '../components/spinner-component/spinner.component';
import { SupportPage } from '../pages/support/support';


@NgModule({
  declarations: [
    ConferenceApp,
    AboutPage,
    AccountPage,
    LoginPage,
    DashboardPage,
    JobPage,
    BuildPage,
    SupportPage,
    TabsPage,
    TutorialPage,
    DisclaimerComponent,
    JobComponent,
    JobHeaderComponent,
    JobHealthComponent,
    JobBuildReportComponent,
    LatestJobsComponent,
    LastUpdateComponent,
    JobStatusComponent,
    JobSubHeaderComponent,
    BuildChangesComponent,
    SpinnerComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    InterceptorModule,
    IonicModule.forRoot(ConferenceApp, {}, {
      links: [
        { component: TabsPage, name: 'TabsPage', segment: 'jenkins-app' },
        { component: DashboardPage, name: 'Dashboard', segment: 'dashboard' },
        { component: JobPage, name: 'JobDetail', segment: 'dashboard/:jobName' },
        { component: BuildPage, name: 'BuildDetail', segment: 'dashboard/:jobName/:buildId' },
        { component: AboutPage, name: 'About', segment: 'about' },
        { component: TutorialPage, name: 'Tutorial', segment: 'tutorial' },
        { component: LoginPage, name: 'LoginPage', segment: 'login' },
        { component: SupportPage, name: 'SupportPage', segment: 'support' },
        { component: AccountPage, name: 'AccountPage', segment: 'account' },
      ]
    }),
    IonicStorageModule.forRoot({
      name: '__ionic-jenkins-db',
      driverOrder: ['websql']
    })
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    ConferenceApp,
    AboutPage,
    AccountPage,
    LoginPage,
    SupportPage,
    DashboardPage,
    JobPage,
    BuildPage,
    TabsPage,
    TutorialPage,
    DisclaimerComponent,
    JobComponent
  ],
  providers: [
    { provide: ErrorHandler, useClass: IonicErrorHandler },
    UserData,
    Auth,
    UserService,
    JenkinsService,
    InAppBrowser,
    SplashScreen
  ]
})

export class AppModule { }

