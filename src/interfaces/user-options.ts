
export interface UserOptions {
  username: string,
  password: string,
  url: string
}
