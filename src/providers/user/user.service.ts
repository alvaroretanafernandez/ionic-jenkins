import { Injectable } from '@angular/core';
import { Auth } from '../auth/auth.service';

@Injectable()
export class UserService {
  
  constructor(public auth: Auth) {
  }
  
  public login(loginData: any): Promise<any> {
    return new Promise((resolve, reject) => {
      let user = {
        username: loginData.username,
        apitoken: loginData.password,
        token: btoa(loginData.username+':'+ loginData.password),
        url: loginData.url
      }
      this.auth.setUser(user)
        .then((_user) => {
          resolve(_user);
        },() => {reject()});
    });
  }
  
}
