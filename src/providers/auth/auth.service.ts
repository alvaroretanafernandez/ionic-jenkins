import { Injectable } from '@angular/core';
import { Storage } from '@ionic/storage';
import { Events } from 'ionic-angular';
@Injectable()
export class Auth {
  
  public user: any ;
  
  constructor(public storage: Storage, public events: Events) {
    this.getUser().then((_user) => {
      if (!!_user) {
        this.user = _user;
      }
    });
  }
  
  /**
   *
   * @returns {Promise<boolean>}
   */
  
  isLoggedIn(): Promise<boolean> {
    return this.storage.get('user')
      .then((_user: any) => {
        if (!!_user) {
          this.user = _user;
          // token valid
          return true;
        } else {
          // no token
          return false;
        }
      });
  }
  
  /**
   *
   * @returns {Promise<TResult>}
   */
  getToken(): Promise<any> {
    return new Promise((resolve, reject) => {
      if (!!this.user) {
        resolve(this.user['api-token']);
      } else {
        reject();
      }
    });
  }
  
  getUser(): Promise<any> {
    return this.storage.get('user')
      .then((_user: any) => {
        if (!!_user) {
          let user = _user;
          return user;
        } else {
          return null;
        }
        
      });
  }
  
  setUser(user: any): Promise<any> {
    return new Promise((resolve, reject) => {
      this.storage.set('user', user)
        .then(() => {
          this.user = user;
          resolve(user);
        }, () => {
          reject()
        });
    });
  }
  
  /**
   *
   */
  destroyAuth(): void {
    this.user = null;
    // TODO: clear just the login
    this.storage.clear();
    this.events.publish('user:logout');
  }
}
