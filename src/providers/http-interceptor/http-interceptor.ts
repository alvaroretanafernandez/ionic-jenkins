import { Injectable } from '@angular/core';
import { HttpEvent, HttpHandler, HttpInterceptor, HttpRequest } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Auth } from '../auth/auth.service';
@Injectable()
export class HttpsRequestInterceptor implements HttpInterceptor {
  
  token: any;
  constructor(public auth: Auth){
    this.auth.getUser().then(user =>{
      this.token = user['token'];
    });
  }
  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    const dupReq = req.clone({
          headers: req.headers.set('authorization', 'Basic ' + this.token)
    });
    return next.handle(dupReq);
  }
};
