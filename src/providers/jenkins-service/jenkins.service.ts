import { Injectable } from '@angular/core';
import { Auth } from '../auth/auth.service';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';

@Injectable()
export class JenkinsService {
  
  JENKINS_URL = '/api/json';
  url: any;
  
  constructor(public auth: Auth, public http: HttpClient) {
    
    this.auth.getUser().then(user =>{
      this.url = user.url;
    });
  }
  
  /**
   * Get Jenkins Dashboard
   * @returns {any}
   */
  public getDashboard(): Observable<any> {
    if (!this.auth.user) {
      return Observable.of({});
    }else{
      let url = this.url + this.JENKINS_URL;
      return this.http.get(url);
    }
  }
  
  /**
   * Get Jenkins Job
   * @param jobName
   * @returns {Observable<Object>}
   */
  public getJob(jobName: any): Observable<any> {
    let url = this.url + '/job/' + jobName + this.JENKINS_URL;
    return this.http.get(url);
  }
  
  /**
   * Trigger Build
   * @param jobName
   * @returns {Observable<Object>}
   */
  public buildJob(jobName: any): Observable<any> {
    let url = this.url + '/job/' + jobName + '/build?token='+ this.auth.user.apitoken;
    return this.http.post(url, {user: 'freepowder', token:this.auth.user.apitoken})
  }
  
  /**
   *  Get Jenkins Build
   * @param jobName
   * @param buildNo
   * @returns {Observable<Object>}
   */
  public getBuild(jobName: any, buildNo: any): Observable<any> {
    let url = this.url + '/job/' + jobName + '/' + buildNo + this.JENKINS_URL;
    return this.http.get(url);
  }
  
  public getBuildLog(jobName: any, buildNo: any) {
    
    let url = this.url + '/job/' + jobName + '/' + buildNo + '/consoleText' + this.JENKINS_URL;
    return this.http.get(url)
      //.map(res => res._body)
  }
}


