import { Component } from '@angular/core';

import { Events, NavController } from 'ionic-angular';

import { Auth } from '../../providers/auth/auth.service';


@Component({
  selector: 'page-account',
  templateUrl: 'account.html'
})
export class AccountPage {
  user: any;
  
  constructor(
    public events: Events,
    public nav: NavController,
    public auth: Auth) {
    this.getUsername();
  }

  ngAfterViewInit() {
   // this.getUsername();
  }

  getUsername() {
    this.auth.getUser().then((user) => {
      this.user = user;
    });
  }

  logout() {
    this.auth.destroyAuth();
  }

 
}
