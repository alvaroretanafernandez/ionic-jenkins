import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, Refresher, ToastController } from 'ionic-angular';
import { JenkinsService } from '../../providers/jenkins-service/jenkins.service';
import { BuildPage } from '../build/build';

@IonicPage({
  segment: 'dashboard/:jobName'
})
@Component({
  selector: 'page-job',
  templateUrl: 'job.html'
})
export class JobPage {
  
  job: any;
  displayName: any;
  lastUpdate: any;
  headerColor: any = 'BUILDING';
  subheaderColor: any = '#488aff';
  constructor(
    public jenkinsService: JenkinsService,
    public navCtrl: NavController,
    public toastCtrl: ToastController,
    public navParams: NavParams
  ) {
    this.getJob();
    this.lastUpdate =new Date();
  }
  
  getJob(){
    this.jenkinsService.getJob(this.navParams.get('jobName'))
      .subscribe((data) =>{
        console.log(data);
        this.job = data;
        this.displayName = data.displayName;
        switch(true){
          case this.job.color ==='blue':
            this.headerColor = 'SUCCESS';
            this.subheaderColor = '#78b037'
            break;
          case  this.job.color ==='red':
            this.headerColor = 'FAILURE';
            this.subheaderColor = '#d54c53'
            break;
          case  this.job.color ==='blue_anime':
            this.headerColor = 'BUILDING';
            break;
          default:
            this.headerColor = 'BUILDING';
            break;
        }
      });
    
  }
  
  buildJob(){
    this.jenkinsService.buildJob(this.navParams.get('jobName'))
      .subscribe(() =>{
      
      },
      error => {
        console.log(error);
        this.job = null;
        this.headerColor = 'BUILDING';
        this.subheaderColor='#488aff';
        setTimeout(() => {
          this.getJob();
          const toast = this.toastCtrl.create({
            message: 'Job is building.',
            duration: 3000
          });
          toast.present();
        }, 2000);
      });
  }
  
  goToBuild(build:any){
    this.navCtrl.push(BuildPage,{jobName:this.job.name,buildId:build});
  }
  
  doRefresh(refresher: Refresher) {
    this.job=null;
    this.getJob();
    setTimeout(() => {
      refresher.complete();
      const toast = this.toastCtrl.create({
        message: 'Job has been updated...',
        duration: 3000
      });
      toast.present();
    }, 1000);
  }
}
