import { Component } from '@angular/core';
import { NgForm } from '@angular/forms';
import { NavController, Events } from 'ionic-angular';
import { UserOptions } from '../../interfaces/user-options';
import { TabsPage } from '../tabs-page/tabs-page';
import { UserService } from '../../providers/user/user.service';

@Component({
  selector: 'page-user',
  templateUrl: 'login.html'
})
export class LoginPage {
  login: UserOptions = {
    username: '',
    password: '',
    url: '' };
  submitted = false;

  constructor(
    public events: Events,
    public navCtrl: NavController,
    public userService: UserService) { }

  onLogin(form: NgForm) {
    this.submitted = true;

    if (form.valid) {
      this.userService.login(this.login)
        .then((_user) => {
          this.events.publish('user:login');
          this.navCtrl.push(TabsPage);
        });
      
    }
  }
}
 // username: 'freepowder',
 // password: '3c378fb1b843fff6a875162d76f8e383',
 // url: 'http://35.176.165.59:8082'
