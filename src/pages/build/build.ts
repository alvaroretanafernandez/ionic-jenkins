import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { JenkinsService } from '../../providers/jenkins-service/jenkins.service';

@IonicPage({
  segment: 'dashboard/:jobName/:buildId'
})
@Component({
  selector: 'page-build',
  templateUrl: 'build.html'
})
export class BuildPage {
  build : any;
  buildLog :any;
  jobName: any;
  buildNo: any;
  headerColor:any = 'BUILDING';
  subheaderColor: any = '#488aff';
  lastUpdate: any;
  
  constructor(public jenkinsService: JenkinsService, public navCtrl: NavController, public navParams: NavParams) {
    this.jobName = this.navParams.get('jobName');
    this.buildNo = this.navParams.get('buildId');
    this.getBuild();
    this.lastUpdate = new Date();
  }
  getBuild() {
    this.jenkinsService.getBuild(this.jobName, this.buildNo)
      .subscribe((data) => {
        this.build=data;
        switch(true){
          case this.build.result ==='SUCCESS':
            this.headerColor = 'SUCCESS';
            this.subheaderColor = '#78b037'
            break;
          case  this.build.result ==='FAILURE':
            this.headerColor = 'FAILURE';
            this.subheaderColor = '#d54c53'
            break;
          default:
            this.headerColor = 'BUILDING';
            break;
        }
      });
    this.jenkinsService.getBuildLog(this.jobName, this.buildNo)
      .subscribe((data) => {
        this.buildLog=data;
      },err =>{
        console.log(err);
        this.buildLog=err.error['text'];
      });
  }
}
