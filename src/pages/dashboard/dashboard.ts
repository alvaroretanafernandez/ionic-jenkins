import { Component, OnInit  } from '@angular/core';

import {  NavController, Refresher, ToastController } from 'ionic-angular';
import { JenkinsService } from '../../providers/jenkins-service/jenkins.service';
import { Auth } from '../../providers/auth/auth.service';
import { JobPage } from '../job/job';


@Component({
  selector: 'page-dashboard',
  templateUrl: 'dashboard.html'
})
export class DashboardPage implements OnInit{
 
  dashboard: any;

  constructor(
    public toastCtrl: ToastController,
    public jenkinsService: JenkinsService,
    public auth: Auth,
    public navCtrl: NavController,
  ) {
    //this.getData();
  }
  
  ngOnInit() {
    this.getData();
  }
  
  getData() {
    this.jenkinsService.getDashboard()
     .subscribe((data) =>{
       this.dashboard = data;
     }, (err) => {
       console.log(err);
     });
  }
  goToJobDetail(job: any) {
    this.navCtrl.push(JobPage, {jobName:job.name, job: job });
  }
 
  doRefresh(refresher: Refresher) {
    this.dashboard=null;
    this.getData();
      setTimeout(() => {
        refresher.complete();
        const toast = this.toastCtrl.create({
          message: 'Jobs have been updated.',
          duration: 3000
        });
        toast.present();
      }, 1000);
  }
}
