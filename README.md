# Jenkins on the move! an Ionic Application

![alt text](src/assets/img/appicon3.png "Logo Title Text 1")


This is purely a demo of Ionic with TypeScript. It is still in development.






## Getting Started

* Clone this repository.
* Run `npm install` from the project root.
* Install the ionic CLI (`npm install -g ionic`)
* Run `ionic serve` in a terminal from the project root.
* Profit





## Use Cases
* login.
* dashboard.
* job view.
* trigger a build.
* log job view.



## Deploying

* PWA - run `npm run ionic:build --prod` and then push the `www` folder to your favorite hosting service
* Android - Run `ionic cordova run android --prod`
* iOS - Run `ionic cordova run ios --prod`

